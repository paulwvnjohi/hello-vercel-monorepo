import { describe, expect, test } from 'vitest';
import { fireEvent, render } from '@testing-library/svelte';
import Counter from '$lib/Counter.svelte';

describe('Counter', () => {
	test('button increments count', async () => {
		const { getByTestId } = render(Counter);
		const button = getByTestId('increase');
		await fireEvent.click(button);

		const count = await getByTestId('count').innerHTML;
		expect(count).toBe('1');
	});

	test('button decreases count', async () => {
		const { getByTestId } = render(Counter);
		const button = getByTestId('decrease');
		await fireEvent.click(button);

		const count = await getByTestId('count').innerHTML;
		expect(count).toBe('-1');
	});
});
